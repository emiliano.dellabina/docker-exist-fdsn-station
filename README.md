# docker-exist-fdsn-station

[![N|Solid](https://www.docker.com/sites/default/files/d8/styles/role_icon/public/2019-07/horizontal-logo-monochromatic-white.png?itok=SBlK2TGU)](https://www.docker.com/sites/default/files/d8/styles/role_icon/public/2019-07/horizontal-logo-monochromatic-white.png?itok=SBlK2TGU)
[![Build Status](http://exist-db.org/exist/apps/homepage/resources/img/existdb.gif)](https://http://exist-db.org/exist/apps/homepage/resources/img/existdb.gif)

This project aims to create a database and implementing fdsn-station webservice.
The database use eXist-db NoSQL Document Database and Application Platform. 

(exist-fdsn-station docker version) 


## Quickstart
### Install docker-compose https://docs.docker.com/compose/install/

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
#sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose #option
docker-compose build
```

## basic docker-compose command-line reference
```
docker-compose up -d #run 
docker-compose down #stop e rm
docker-compose stop #stop
docker-compose start #start no run
```

## ssk-copy-id
```
ssh-keygen -f "~/.ssh/known_hosts" -R "[localhost]:2222"
ssh-copy-id -i ~/.ssh/id_rsa root@localhost -p 2222

```

## ssh
```
ssh -Y -p '2222' 'root@localhost' # using "-Y" param to use Java Admin Client
```

## Basic install ubuntu 18.04.4 update/upgrade
```
apt update
apt upgrade -y
apt install iproute2 wget vim nano -y
```


## Install eXistdb
```
apt install openjdk-8-jre-headless -y
wget https://bintray.com/existdb/releases/download_file?file_path=exist-installer-5.2.0.jar  -O exist-installer-5.2.0.jar
#sudo adduser --system --shell /sbin/nologin --comment "eXist-db Service Account" existdb
java -jar exist-installer-5.2.0.jar -console
```

## EXistdb start/stop cli
```
sh /usr/local/eXist-db/bin/startup.sh &
sh /usr/local/eXist-db/bin/shutdown.sh &
```

## Java Admin Client - eXistDB Gui-tools to manage user, db-import/export
```
sh /usr/local/eXist-db/bin/client.sh &
```

## Troubleshot

Error java.awt.AWTError: Assistive Technology not found

This can be done by editing the accessibility.properties file for OpenJDK:
```
sudo vim /etc/java-8-openjdk/accessibility.properties
```

Comment out the following line:
```
assistive_technologies=org.GNOME.Accessibility.AtkWrapper
```

## Reference
https://exist-db.org/exist/apps/doc/tuning
